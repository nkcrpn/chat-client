import java.io.*;
import java.net.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.sound.midi.*;

public class SimpleChatClient {
	JTextArea incoming;
	JTextField outgoing, userName;
	BufferedReader reader;
	PrintWriter writer;
	Socket sock;
	
	Sequencer player;
	
	public static void main(String[] args){
		SimpleChatClient client = new SimpleChatClient();
		client.setUpMidi();
		client.go();
	}
	
	public void setUpMidi(){
		try {
			player = MidiSystem.getSequencer();
			player.open();
			
			Sequence seq = new Sequence(Sequence.PPQ, 4);
			Track track = seq.createTrack();
			
			ShortMessage a = new ShortMessage();
			a.setMessage(144, 1, 60, 100);
			MidiEvent noteOn = new MidiEvent(a, 1);
			track.add(noteOn);
			
			ShortMessage b = new ShortMessage();
			b.setMessage(128, 1, 60, 100);
			MidiEvent noteOff = new MidiEvent(b, 4);
			
			
			track.add(noteOff);
			
			player.setSequence(seq);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void go() {
		JFrame frame = new JFrame("Simple Chat Client");
		JPanel mainPanel = new JPanel();
		incoming = new JTextArea(15, 50);
		incoming.setLineWrap(true);
		incoming.setWrapStyleWord(true);
		incoming.setEditable(false);
		
		JScrollPane qScroller = new JScrollPane(incoming);
		qScroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		qScroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		JLabel uN = new JLabel("ID: ");
		JLabel txtLbl = new JLabel("Message: ");
		outgoing = new JTextField(20);
		outgoing.addActionListener(new SendButtonListener());
		userName = new JTextField(10);
		JButton sendButton = new JButton("Send");
		sendButton.addActionListener(new SendButtonListener());
		mainPanel.add(qScroller);
		mainPanel.add(uN);
		mainPanel.add(userName);
		mainPanel.add(txtLbl);
		mainPanel.add(outgoing);
		mainPanel.add(sendButton);
		setUpNetworking();
		
		Thread readerThread = new Thread(new IncomingReader());
		readerThread.start();
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(BorderLayout.CENTER, mainPanel);
		frame.setSize(610, 350);
		frame.setVisible(true);
		
	}
	
	private void setUpNetworking() {
		try {
			/* Only works on local computer for now. Change "localhost"
			 * to correct ip address and 5000 to correct port.
			 */
			sock = new Socket("localhost", 5000);
			InputStreamReader streamReader = new InputStreamReader(sock.getInputStream());
			reader = new BufferedReader(streamReader);
			writer = new PrintWriter(sock.getOutputStream());
			System.out.println("networking established");
			System.out.println(sock.getLocalSocketAddress());
			System.out.println(sock.getRemoteSocketAddress());
		} catch (IOException e) {
			e.printStackTrace();
			incoming.append("Failure");
		}
	}
	
	public class SendButtonListener implements ActionListener {
		public void actionPerformed (ActionEvent event) {
			try {
				writer.println(userName.getText() + " - " + outgoing.getText());
				writer.flush();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			outgoing.setText("");
			outgoing.requestFocus();
		}
	}
	
	public class IncomingReader implements Runnable {
		public void run() {
			String message;
			try {
				while ((message = reader.readLine()) != null) {
					player.stop();
					player.setTickPosition(0);
					System.out.println("read " + message);
					incoming.append(message + "\n");
					if (message.contains("BEEP") || message.contains("SERVER")) {
						player.start();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
}